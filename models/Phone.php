<?php
namespace app\models;

use yii\db\ActiveRecord;

class Phone extends ActiveRecord
{
    /**
     * @return string название таблицы, сопоставленной с этим ActiveRecord-классом.
     */
    public static function tableName()
    {
        return '{{phones}}';
    }

    public function rules()
    {
      	return [
            ['contact_id', 'required'],
            ['phone', 'required'],
            ['phone', 'match', 'pattern' => '/^(\+)?(\d|\s|-|\(|\))*$/'],
      	];
    }

    /**
     * @return bool Перед сохранением телефона устанавливаем дату обновления.
     */
    public function beforeSave($insert)
    {
      if (!parent::beforeSave($insert)) {
          return false;
      }

      if (!$this->id) {
        $this->created_at = time();
      }

      $this->updated_at = time();

      return true;
    }
}
