<?php
namespace app\models;

use yii\db\ActiveRecord;

class Contact extends ActiveRecord
{
    /**
     * @return string название таблицы, сопоставленной с этим ActiveRecord-классом.
     */
    public static function tableName()
    {
        return '{{contacts}}';
    }

    public function rules()
    {
      	return [
            ['user_id', 'required'],
            ['name', 'required'],
            [['surname', 'patronymic'], 'string'],
      	];
    }


    /**
     * @return Contact Получение контакта по идентификатору.
     */
    public static function getContact($id)
    {
        return self::find()->where(['id' => $id])->one();
    }

    /**
     * @return string Получение полного имени.
     */
    public function getFullname()
    {
      $fullname = [];
      if (isset($this->surname) && $this->surname) {
        $fullname[] = $this->surname;
      }
      if (isset($this->name) && $this->name) {
        $fullname[] = $this->name;
      }
      if (isset($this->patronymic) && $this->patronymic) {
        $fullname[] = $this->patronymic;
      }
      return join(' ', $fullname);
    }

    /**
     * @return ActiveQuery Связь один ко многим с Phone model.
     */
    public function getPhones()
    {
        return $this->hasMany(Phone::classname(), ['contact_id' => 'id']);
    }

    /**
     * @return void Очистить привязанные телефоны к контакту устанавливаем дату обновления.
     */
    public function clearPhones()
    {
      $this->unlinkAll('phones', true);
    }

    /**
     * @return bool Перед удалением контакта.
     */
    public function beforeDelete()
    {
       if (!parent::beforeDelete()) {
           return false;
       }

       $this->clearPhones();
       return true;
    }

    /**
     * @return bool Перед сохранением контакта.
     */
    public function beforeSave($insert)
    {
      if (!parent::beforeSave($insert)) {
          return false;
      }

      if (!$this->id) {
        $this->created_at = time();
      }

      $this->updated_at = time();

      return true;
    }

}
