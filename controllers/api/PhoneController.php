<?php
namespace app\controllers\api;

use yii\rest\ActiveController;

class PhoneController extends ActiveController
{
    public $modelClass = 'app\models\Phone';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
}
